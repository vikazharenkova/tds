// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS/FuncLibrary/MyTypes.h"
#include "TPS/Weapon/WeaponDefault.h"
#include "TPS/Character/TPSInventoryComponent.h"
#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "TPS/Interface/TPS_IGameActor.h"
#include "TPS/StateEffects/TPS_StateEffect.h"
//#include "Components/WidgetComponent.h"

#include "TPSCharacter.generated.h"

//USTRUCT(BlueprintType)
//struct FCharacterSpeedInfo
//{
//	GENERATED_BODY()
//
//
//};
UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPS_IGameActor
{
	GENERATED_BODY()

protected:

	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	virtual void BeginPlay() override;

	//Inputs
	void InputAxisY(float Value);
	void InputAxisX(float Value);

	void InputAttackPressed();
	void InputAttackReleased();

	void InputWalkPressed();
	void InputWalkReleased();

	void InputSprintPressed();
	void InputSprintReleased();

	void InputAimPressed();
	void InputAimReleased();

	//Inventory Inputs
	void TrySwitchNextWeapon();
	void TrySwitchPreviosWeapon();
	//Ability Inputs
	void TryAbilityEnabled();

	template<int32 Id>
	void TKeyPressed()
	{
		TrySwitchWeaponToIndexByKeyInput_OnServer(Id);
	}
	//Inputs End

	//Input Flags
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	bool SprintRunEnabled = false;
	bool WalkEnabled = false;
	UPROPERTY(BlueprintReadOnly)
	bool AimEnabled = false;

	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;

	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(Replicated)
	TArray<UTPS_StateEffect*> Effects;
	UPROPERTY(ReplicatedUsing=EffectAdd_OnRep)
	UTPS_StateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTPS_StateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;



	UPROPERTY(Replicated)
	int32 CurrentIndexWeapon = 0;

	UFUNCTION()
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;


public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UTPSInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* CharHealthComponent;

	//Cursor material on decal
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);
	//Default move rule and state character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPS_StateEffect> AbilityEffect;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	//Cursor
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	//	UMaterialInterface* CursorMaterial = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
	//	FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	EMovementState MovementState = EMovementState::Run_State;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	FCharacterSpeed MovementSpeedInfo;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	bool SprintRunEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	bool WalkEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	bool AimEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	bool bIsAlive = true;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	TArray<UAnimMontage*> DeadsAnim;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	//	TSubclassOf<UTPS_StateEffect> AbilityEffect;

	////Weapon	
	//AWeaponDefault* CurrentWeapon = nullptr;

	////for demo 
	////UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	////	FName InitWeaponName;

	//UDecalComponent* CurrentCursor = nullptr;

	////Effect
	//TArray<UTPS_StateEffect*> Effects;

	////Inputs
	//UFUNCTION()
	//	void InputAxisY(float Value);
	//UFUNCTION()
	//	void InputAxisX(float Value);
	//UFUNCTION()
	//	void InputAttackPressed();
	//UFUNCTION()
	//	void InputAttackReleased();

	//float AxisX = 0.0f;
	//float AxisY = 0.0f;

	// Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	//UFUNCTION(BlueprintCallable)
	//	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
		//void InitWeapon(FName IdWeaponName);



	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* AnimReload);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* AnimFire);

	UFUNCTION(Server, Reliable)
	void TrySwitchWeaponToIndexByKeyInput_OnServer(int32 ToIndex);

	void DropCurrentWeapon();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* AnimReload);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* AnimFire);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTPS_StateEffect*> GetCurrentEffectsOnChar();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	//Inventory Func
	//void TryAbilityEnabled();
	//void TrySwicthNextWeapon();
	//void TrySwitchPreviosWeapon();

	//UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	//	int32 CurrentIndexWeapon = 0;

	//Interface
	EPhysicalSurface GetSurfuceType() override;
	TArray<UTPS_StateEffect*> GetAllCurrentEffects() override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPS_StateEffect* RemoveEffect);
	void RemoveEffect_Implementation(UTPS_StateEffect* RemoveEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPS_StateEffect* newEffect);
	void AddEffect_Implementation(UTPS_StateEffect* newEffect) override;
	//End Interface

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);


	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
		void PlayAnimDead_Multicast(UAnimMontage* AnimDead);


	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();


	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
		void SwitchEffect(UTPS_StateEffect* Effect, bool bIsAdd);

	/*UFUNCTION()
	void CharDead();
	void EnableRagdoll_Multicast();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;*/




	//UFUNCTION(BlueprintCallable)//VisualOnly
//	void RemoveCurrentWeapon();

};

